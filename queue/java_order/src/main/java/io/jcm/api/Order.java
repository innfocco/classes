package io.jcm.api;

import java.util.UUID;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(value = "Product order service")
@RestController
@RequestMapping("/order")
public class Order {
	
	@ApiOperation(value = "Serviço de exemplo Compra de produto", notes = "Todos os valores são aceitos", response = String.class, tags={ "store", })
    @ApiResponses(value={
	        @ApiResponse(code = 400, message = "Quantidade inválida", response = String.class),
	        @ApiResponse(code = 404, message = "Produto Indisponível", response = String.class) 
        })
	@RequestMapping(value="/buy/{product}/{amount}", method=RequestMethod.GET)
    public String buy(@PathVariable String product,	@PathVariable Integer amount) {
		try{
			String dummyJson = "{\"hash\":\"" + UUID.randomUUID().toString() + "\", \"product\":\"" + product + "\", \"amount\":\"" + amount + "\"}";
			MqttClient client = new MqttClient("tcp://172.23.222.41:1883", "java-app");
			//MqttClient client = new MqttClient("tcp://broker.hivemq.com:1883", "java-app");
			client.connect();
			MqttMessage message = new MqttMessage();
			message.setPayload(dummyJson.getBytes());
			client.publish("v1/orders", message);
			client.disconnect();
		} catch (Exception e){
			e.printStackTrace();
		}
        return "Vendido " + amount + " " + product;
    }
	
	
	@ApiOperation(value = "Serviço de teste para conferir se a app subiu" , response = String.class, tags={ "hello", })
	@RequestMapping(value="/hello", method=RequestMethod.GET)
    public String home() {
        return "Spring Boot is running!";
    }

} //http://localhost:8080/v1/swagger-ui.html
 //http://www.mqtt-dashboard.com/
