const mqtt = require('mqtt');
const client = mqtt.connect('mqtt://172.23.222.41');
//const client = mqtt.connect('mqtt://broker.hivemq.com');

/**
a partir do cliente é possivel fazer
client.publish('v1/orders', 'Hello mqtt')
e também 
client.end()
**/

client.on('connect', function () {
  client.subscribe('v1/orders')
  console.log('Esperando por pedidos... ')
})
 
client.on('message', function (topic, message) {
  var order = JSON.parse(message.toString());
  console.log("pedido recebido: " + order.hash)
  console.log("Produro: " + order.product)
  console.log("Quantidade: " + order.amount)
})
